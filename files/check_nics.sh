#!/bin/sh
#
# check_nics.sh: Script checks status and RX errors of NIC interfaces.
#
# Copyright (C) 2009 Institute of Computer Science, Masaryk University
# Author(s):	Pavel CELEDA	<celeda@ics.muni.cz>
#		Tomas PLESNIK	<plesnik@ics.muni.cz>
#
# LICENSE TERMS - 3-clause BSD license
#
# Attention:
# - flowmon must be set up in sudoers for executing ethtool without password
# - nrpe service must be execute as a user flowmon
# - howto reset ifconfig counters via 
#   http://www.ducea.com/2006/09/08/resetting-ifconfig-counters/
#
# $Id: check_nics.sh 3224 2014-11-11 15:34:43Z 207426 $
#
export LC_ALL=C
VERSION="$Id: check_nics.sh 3224 2014-11-11 15:34:43Z 207426 $"

#-------------------------------------------------------------------------------
#                               FUNCTIONS
#-------------------------------------------------------------------------------
usage()
{
        echo "Usage: ${0##*/} -i \"<interfaces>\" [-hV]"
        echo "-i \"<interfaces>\"	list of checked network interfaces"
        echo "-h                	print this help"
        echo "-V                	print script version number and exit"
        echo
        echo "Example: $ ./${0##*/} -i \"eth2 eth3\""
        exit 0
}

version()
{
        echo "${0##*/} $VERSION"
        exit 0
}

#-------------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------------
while getopts "i:Vh" options; do
        case "$options" in
                i ) I_OPT="$OPTARG";;
                h ) usage;;
                V ) version;;
                * ) usage;;
        esac
done

if [ -z "$I_OPT" ]; then
        echo "Parameter '-i \"<interfaces>\"' is not set!"
        echo
        usage
	exit 1
fi

# list of interfaces to check
INTERFACES="$I_OPT"

# NIC status variables
NICSTATUS=""
LINKDOWN=0

# RX errors variables
ERRORSTATUS=""
RX_ERRS_TRESHOLD=100
ERRORS=0

# read nagios return codes
# PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'` # CHANGED
PROGPATH=/usr/lib64/nagios/plugins # CHANGED
. $PROGPATH/utils.sh

# ethtool execute variable
if which ethtool 1>/dev/null 2>&1; then
	ETHTOOL=`which ethtool`
else
	echo "Error: ethtool is not installed!"
        exit 2
fi

# ifcondig execute variable
if which ifconfig 1>/dev/null 2>&1; then
	IFCONFIG=`which ifconfig`
else
	echo "Error: ifconfig is not installed!"
        exit 3
fi

# check link state and sum of RX errors for each nic
for INTERFACE in $INTERFACES; do
	# check if interface exists
	if [ ! `ls -1 /sys/class/net | grep $INTERFACE` ]; then
		echo "Error: network interface '$INTERFACE' is not exists!"
		exit 4
	fi

	# check status of both tasks
        LINKSTATUS=`ethtool $INTERFACE | grep "Link detected" | cut -d: -f 2 | cut -d " " -f 2`
	RX_ERRORS=`$IFCONFIG -s $INTERFACE | grep $INTERFACE | awk '{print $4}'`

	# check link status
	if [ $LINKSTATUS == "yes" ]; then
	        NICSTATUS=${NICSTATUS}" "${INTERFACE}" (up)"
	else
	        NICSTATUS=${NICSTATUS}" "${INTERFACE}" (down)"
		let LINKDOWN++
	fi

	# enumerate RX errors 
	if [ $RX_ERRORS -gt $RX_ERRS_TRESHOLD ]; then
		ERRORSTATUS=${ERRORSTATUS}" "${INTERFACE}" ("${RX_ERRORS}" errors)"
		let ERRORS++
	fi
done

# count status code
STATUS_SUM=$[$LINKDOWN + $ERRORS]

# assign exit state STATE_OK=0, STATE_WARNING=1, STATE_CRITICAL=2, STATE_UNKNOWN=3, STATE_DEPENDENT=4
case $STATUS_SUM in
	0)
		EXIT_STATE=$STATE_OK          # all links up and 0 RX errors
		;;
	1)
		EXIT_STATE=$STATE_WARNING     # single link down or some RX errors
		;;
	*)
		EXIT_STATE=$STATE_CRITICAL	# more links down or more RX erros
		;;
esac

# check if some interface has RX errors, if not set "0 errors"
if [ -z "$ERRORSTATUS" ]; then
	ERRORSTATUS="(0 errors)"
fi

# message which will nagios display
echo "NIC_STATUS = $NICSTATUS, RX_ERRORS_STATUS = $ERRORSTATUS, EXIT = $EXIT_STATE"

# exit state which will nagios use to display link status
exit $EXIT_STATE
