#!/bin/bash
#
# check_nfcapd.sh: Script checks an empty NetFlow channels
#
# Copyright (C) 2011 Institute of Computer Science, Masaryk University
# Author(s):	Tomas Plesnik <plesnik@ics.muni.cz>
#
# LICENSE TERMS - 3-clause BSD license
#
# $Id: check_nfcapd.sh 3225 2014-11-11 15:40:12Z 207426 $
#
export LC_ALL=C
VERSION="$Id: check_nfcapd.sh 3225 2014-11-11 15:40:12Z 207426 $"


#-------------------------------------------------------------------------------
#				FUNCTIONS
#-------------------------------------------------------------------------------
usage()
{
        echo "Usage: ${0##*/} -c \"<channels>\" [-hV]"
        echo "-c \"<channels>\"		list of checked NetFlow channels"
        echo "-h                	print this help"
        echo "-V                	print script version number and exit"
        echo
        echo "Example: $ ./${0##*/} -c \"p3000 p3001 p3002 p3003\""
        exit 0
}

version()
{
        echo "${0##*/} $VERSION"
        exit 0
}

#-------------------------------------------------------------------------------
#				MAIN
#-------------------------------------------------------------------------------
while getopts "c:Vh" options; do
        case "$options" in
                c ) C_OPT="$OPTARG";;
                h ) usage;;
                V ) version;;
                * ) usage;;
        esac
done

if [ -z "$C_OPT" ]; then
	echo "Parameter '-c \"<channels>\"' is not set!"
	echo
	usage
	exit 1
fi

# list of channels to check
CHANNELS="$C_OPT"
CHANNEL_STATUS=""
EMPTY_FILES=0

# read nagios return codes
# PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'` # CHANGED
PROGPATH=/usr/lib64/nagios/plugins # CHANGED
. $PROGPATH/utils.sh

# nfdump execute variable
NFDUMP=/usr/local/bin/nfdump
# if which nfdump 1>/dev/null 2>&1; then
# 	NFDUMP=`which nfdump`
# else
# 	echo "Error: nfdump is not installed!"
# 	exit 2
# fi

# check size of last stored nfcapd file of each channel
for CHANNEL in $CHANNELS; do
	NFPATH="/data/nfsen/profiles-data/live/${CHANNEL}/"
	if [ ! -d $NFPATH ]; then
		echo "Error: NetFlow channel '${CHANNEL}' is not a directory!"
		exit 3
	fi
	YEAR=`ls -1 ${NFPATH} | grep -v nfcapd.current | tail -n1`
	MONTH=`ls -1 "${NFPATH}/${YEAR}" | tail -n1`
	DAY=`ls -1 "${NFPATH}/${YEAR}/${MONTH}" | tail -n1`
	NFCAPD_FILE=`ls -1 "${NFPATH}/${YEAR}/${MONTH}/${DAY}" | tail -n1`
	FLOWS=`$NFDUMP -I -r "${NFPATH}/${YEAR}/${MONTH}/${DAY}/${NFCAPD_FILE}" | grep Flows: | cut -f 2 -d " "`

	if [ $FLOWS -eq 0 ]; then
		let EMPTY_FILES++
	fi
	CHANNEL_STATUS=${CHANNEL_STATUS}" "${CHANNEL}" ("${FLOWS}" flows)"
done

# assign exit state STATE_OK=0, STATE_WARNING=1, STATE_CRITICAL=2, STATE_UNKNOWN=3, STATE_DEPENDENT=4
case $EMPTY_FILES in
        0)
                EXIT_STATE=$STATE_OK          # all nfcapd files aren't empty
                ;;
        1)
                EXIT_STATE=$STATE_WARNING     # single nfcapd file is empty
                ;;
        *)
                EXIT_STATE=$STATE_CRITICAL    # more nfcapd files are empty
                ;;
esac

# message which will nagios display
echo "CHANNEL STATUS = $CHANNEL_STATUS, EXIT = $EXIT_STATE"

# exit state which will nagios use to display nfcapd file saturation
exit $EXIT_STATE
