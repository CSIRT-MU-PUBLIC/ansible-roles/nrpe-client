# Ansible role - Nagios client #

This role serves for automatic installation of nrpe-client.

The role will perform full installation.

## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Role paramaters

optional

* `nrpe_client_commands` - List of monitoring commands. Each command consists of following 2 attributes.
    * `name` - The name of the command.
    * `command` - The command itself.
* `nrpe_client_plugins` - List of plugins that will be copied to remote host.
* `nrpe_client_configuration` - Object of [configuration file](https://github.com/NagiosEnterprises/nrpe/blob/master/sample-config/nrpe.cfg.in) where keys are the names of configuration fields.

defaults

* `nrpe_client_default_commands` - Essentially the same as `nrpe_client_commands`, but with some predefined monitoring commands. If you want to disable them all, just set this parameter to empty list (i.e. `[]`).
* `nrpe_client_default_plugins` - Essentially the same as `nrpe_client_plugins`, but with some predefined plugins. If you want to disable them all, just set this parameter to empty list (i.e. `[]`).

## Example

Example of the simplest Nagios client installation.

```yml
roles:
    - role: nrpe-client
      become: yes
```

More specific Nagios client installation.

```yml
roles:
    - role: nrpe-client
      nrpe_client_default_commands: []
      nrpe_client_commands:
          - name: check_memnrpe_client
            command: "/usr/lib/nagios/plugins/check_mem.pl -u -w 90 -c 100"
      nrpe_client_plugins:
          - /path_to_plugins/check_mem.pl
      nrpe_client_configuration:
          allowed_hosts: 172.16.0.11,ex.example.com
      become: yes
```
